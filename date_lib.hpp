/*
 * date_lib.hpp
 *
 *  Created on: 5 июня 2015
 *      Author: strangeman
 */

#ifndef DATE_LIB_HPP_
#define DATE_LIB_HPP_

#include <iostream>
#include <time.h>

namespace CharVector {
	struct ChVector;
}

namespace Dates {

struct Date {
	int m_day, m_month, m_year;
};

enum DateEndian {
	/*
	 * Big-endian - [year, month, day]
	 * Little-endian - [day, month, year]
	 * Middle-endian - [month, day, year]
	 */
	UNDEF = -1, LITTLE, MIDDLE, BIG
};

enum ExtractStatus {
	BAD_INPUT_FORMAT = -2, BAD_DATE, OK, TOO_MUCH_NUMBERS, NOT_ONLY_NUMBERS
};

bool IsLeapYear( const int _year );

bool IsDateValid( const Date & _date );

int GetMaxDaysInMonth( const Date & _date );

ExtractStatus ExtractDate( const char * _cString, Date & _date, DateEndian _endian = UNDEF, const char * _dateSeparators = " .-/" );

ExtractStatus ExtractDate( const char * _cString, tm   & _date, DateEndian _endian = UNDEF, const char * _dateSeparators = " .-/" );

void   GetTextDate( const Date & _date, CharVector::ChVector & _string );

char * GetTextDate( const Date & _date );

}// end of Dates

#endif /* DATE_LIB_HPP_ */
