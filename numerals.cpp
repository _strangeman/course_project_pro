/*
 * numerals.cpp
 *
 *  Created on: 7 июня 2015
 *      Author: strangeman
 */

#include "numerals.hpp"
#include <cassert>

namespace Numerals {

const char * g_cardinalsOnes [] = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
const char * g_cardinalsTeens[] = { "zero", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
const char * g_cardinalsTens [] = { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

const char * g_ordinalsOnes[] = { "null", "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth" };
const char ** g_ordinalsTeens = nullptr;
const char * g_ordinalsTens[] = { "null", "tenth", "twentieth", "thirtieth", "fortieth", "fiftieth", "sixtieth", "seventieth", "eightieth", "ninetieth" };

unsigned int SimpleAbs ( int _num ) {
	return (_num < 0) ? _num * -1 : _num;
}

void SmallNumsToCharVector( int _num, CharVector::ChVector & _dest, bool _getOrdinal ) {
	// Only positive
	assert( _num >= 0 );

	if( _num > 999 ) {
		assert( ! "Dedicated only for numbers less than one thousand" );
		return;
	}

	if( _num > 99 ) {
		int butHundreds = _num % 100;
		CharVector::Concatenate( _dest, g_cardinalsOnes[ _num / 100 ] );

		if ( _getOrdinal ) {
			if ( ! butHundreds ) {
				CharVector::Concatenate( _dest, "-hundredth" );
			}
			else {
				CharVector::Concatenate( _dest, " hundred and " );
				SmallNumsToCharVector( butHundreds, _dest, true );
			}
		}
		else {
			CharVector::Concatenate( _dest, " hundred" );
			if ( butHundreds )
				CharVector::Concatenate( _dest, " " );

			SmallNumsToCharVector( butHundreds, _dest, false );
		}

	}
	else if( _num > 19 ) {

		CharVector::Concatenate( _dest, g_cardinalsTens[ _num / 10 ] );
		if( _getOrdinal) {
			if ( _num % 10 ) {
				CharVector::Concatenate( _dest, "-");
				CharVector::Concatenate( _dest, g_ordinalsOnes [ _num % 10 ] );
			}
		}

		else {
			if ( _num % 10 ) {
				CharVector::Concatenate( _dest, "-");
				CharVector::Concatenate( _dest, g_cardinalsOnes [ _num % 10 ] );
			}
		}
	}
	else if( _num > 9 ) {
		CharVector::Concatenate( _dest, g_cardinalsTeens[ _num - 9 ] );
		if( _getOrdinal)
			CharVector::Concatenate( _dest, "th" );

	}
	else if( _num > 00 ) {
		if( _getOrdinal )
			CharVector::Concatenate( _dest, g_ordinalsOnes [ _num ] );

		else
			CharVector::Concatenate( _dest, g_cardinalsOnes[ _num ] );
	}
}

void NumeralToCharVector  ( const int _num, CharVector::ChVector & _dest, bool _getOrdinal ) {
	// Постфиксы для различных степеней тысячи
	const char * postfixes[] = {" billion", " million", " thousand", "" };

	// Переменная для хранения текущей степени тысячи. Начинаем с миллиарда
	int currPowerOf1000 = 1000000000;

	// Копируем анализируемое число в рабочую переменную
	int currNum = _num;

	// Если число меньше нуля - дописываем "минус" и обрабатываем абсолютное значение
	if( _num < 0 ) {
		CharVector::Concatenate( _dest, "minus " );
		currNum = SimpleAbs( currNum );
	}

	// Циклически получаем текстовый эквивалент для степеней тысячи
	for( int i = 0, currPowersInNum; currNum; i++ ) {
		// Анализируем разряды, которые соотвествуют текущей степени тысячи
		currPowersInNum = currNum / currPowerOf1000;

		// Если они не равны нулю...
		if( currPowersInNum ) {
			// ...то получаем их текстовый эквивалент...
			SmallNumsToCharVector( currPowersInNum, _dest, false );
			// ... и дописываем постфикс
			CharVector::Concatenate( _dest, postfixes[ i ] );

			/*
			 * Если нас попросили получить порядковое числительное
			 * и все меньшие по разряду числа равны нулю -
			 * дописываем в конец "th" и выходим
			 */
			if( _getOrdinal && ! ( currNum % currPowerOf1000) ) {
				CharVector::Concatenate( _dest, "th" );
				return;
			}

			// Иначе дописываем пробел
			CharVector::Concatenate( _dest, " " );
		}

		// Отбрасываем проанализированные разряды
		currNum %= currPowerOf1000;

		// Получаем следующую степень тысячи
		currPowerOf1000 /= 1000;
	}

	/*
	 * Получаем текстовый эквивалент для отсавшихся разрядов
	 * после обработки всех предыдущих (т.е. после
	 * обработки разрядов от миллиарда до тысячи)
	 */
	if( currNum ) {
		SmallNumsToCharVector( currNum, _dest, _getOrdinal );
	}
}

} //End of Numerals namespace
