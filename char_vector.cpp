#include <iostream>
#include <cstring>
#include <cassert>

#include "char_vector.hpp"

namespace CharVector {

void Init (ChVector & _s, int _size) {
	_s.m_pData = new char[_size];
	_s.m_nUsed = 1;
	_s.m_nAllocated = _size;
	Clear( _s );
}

void Destroy (ChVector & _s) {
	delete [] _s.m_pData;
	_s.m_pData = nullptr;
}

void Clear (ChVector & _s) {
	_s.m_nUsed = 1;
	memset (_s.m_pData, '\0', _s.m_nAllocated);
}

void Grow (ChVector & _s, int _sizeRequested) {
	int newSize;
	if (_sizeRequested < _s.m_nAllocated) {
		//По умолчанию увеличиваем длину строки на 10 символов
		newSize = _s.m_nAllocated + 10;
	}
	else {
		//Иначе округляем запрошенный размер до ближайшего большего десятка
		newSize = (_sizeRequested / 10 + 1) * 10;
	}
	ChVector newString;
	Init(newString, newSize);
	strcpy (newString.m_pData, _s.m_pData );
	newString.m_nUsed = _s.m_nUsed;
	Destroy(_s);
	_s.m_pData = newString.m_pData;
	_s.m_nAllocated = newSize;
}

void PushBack ( ChVector & _s, const char _ch ) {
	if (_s.m_nUsed == _s.m_nAllocated)
		Grow(_s, _s.m_nAllocated * 2);

	if ( _s.m_nUsed == 0)
		_s.m_pData[_s.m_nUsed ++] = _ch;

	else
		_s.m_pData[_s.m_nUsed - 1] = _ch;

	_s.m_pData[_s.m_nUsed ++] = '\0';
}

void GetUntil (ChVector & _s, std::istream & _stream, const char * _terminator) {
	char buf = 'c';
	const int nOfTerminators = strlen (_terminator);
	while ( _stream.get(buf) ) {

		for (int i = 0; i < nOfTerminators; i++)
			if (_terminator[i] == buf)
				return;

		PushBack (_s, buf);
	}

}

bool IsDiffer (const ChVector & _one, const ChVector & _two) {
	if (_one.m_nUsed != _two.m_nUsed)
		return true;

	return ! static_cast < bool > ( strcmp ( _one.m_pData, _two.m_pData)  );
}

void Print (const ChVector & _s, std::ostream & _stream, char _separator) {
	_stream << _s.m_pData << _separator;
}

void CopyAndReplace (ChVector & _dest, const ChVector & _source) {
	if ( _source.m_nUsed > _dest.m_nAllocated )
		Grow ( _dest, _source.m_nUsed );

	strcpy ( _dest.m_pData, _source.m_pData );
	_dest.m_nUsed = _source.m_nUsed;
}

void CopyAndReplace (ChVector & _dest, const char * _source) {
	int sourceStrLen = strlen(_source);

	if ( sourceStrLen > _dest.m_nAllocated )
		Grow ( _dest, sourceStrLen + _dest.m_nAllocated );

	strcpy ( _dest.m_pData, _source );
	_dest.m_nUsed = sourceStrLen;
}

void Concatenate (ChVector & _dest, const ChVector & _source) {
	assert( _dest.m_nUsed == (strlen( _dest.m_pData ) + 1 ) );
	int neededStrLen = _source.m_nUsed + _dest.m_nUsed - 1;

	if ( neededStrLen > _dest.m_nAllocated )
		Grow ( _dest, neededStrLen + _dest.m_nAllocated );

	strcat( _dest.m_pData, _source.m_pData );

	_dest.m_nUsed = neededStrLen;
}

void Concatenate (ChVector & _dest, const char * _source) {
	assert( _dest.m_pData[ _dest.m_nUsed - 1 ] == '\0' );
	assert( _dest.m_nUsed == (strlen( _dest.m_pData ) + 1 ) );

	int neededStrLen = strlen( _source ) + _dest.m_nUsed;

	if ( neededStrLen > _dest.m_nAllocated )
		Grow ( _dest, neededStrLen + _dest.m_nAllocated );

	strcat( _dest.m_pData, _source );

	_dest.m_nUsed = neededStrLen;
}

bool HasOnlyDigits( const char * _cString ) {
	const int stringLen = strlen( _cString );
	for( int i = 0; i < stringLen; i++ ) {
		if( ! isdigit( _cString[ i ] ) )
			return false;

	}

	return true;
}

bool HasOnlyDigits( const ChVector _string ) {
	for( int i = 0; i < _string.m_nUsed; i++ ) {
		if( ! isdigit( _string.m_pData[ i ] ) )
			return false;

	}

	return true;
}
} //end of CharVector namespace
